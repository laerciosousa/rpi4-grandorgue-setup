#!/bin/sh

HAS_ARGONONE=false
HAS_WAVESHARE_LCD7=true
GRANDORGUE_RELEASE=3.6.5

AUTOSTART=~/.config/autostart
[ -d ${AUTOSTART} ] || mkdir -p ${AUTOSTART}

update_system () {
  echo ">>> Updating system..."
  sudo apt-get update
  sudo apt-get -y upgrade
}

cleanup_system () {
  echo ">>> Cleaning up system..."
  sudo apt-get -y autoremove --purge
  sudo apt-get clean
}

setup_argonone () {
  echo ">>> Install scripts for Argon One case..."
  sh ./argon1.sh
}

setup_waveshare_lcd7 () {
  echo ">>> Set Waveshare LCD7 native screen resolution..."
  cat <<EOF >/tmp/99waveshare-lcd7
xrandr --newmode 1024x600 48.96 1024 1064 1168 1312 600 601 604 622 -HSync +VSync
xrandr --addmode HDMI-1 1024x600
xrandr --addmode HDMI-2 1024x600
xrandr --output HDMI-1 --mode 1024x600
xrandr --output HDMI-2 --mode 1024x600
EOF
  sh /tmp/99waveshare-lcd7
  sudo mv /tmp/99waveshare-lcd7 /etc/X11/Xsession.d

  echo ">>> Install virtual keyboard..."
  sudo apt-get -y install onboard mousetweaks at-spi2-core
  gsettings set org.onbard.auto-show enabled true
  cat <<EOF >${AUTOSTART}/onbaord.desktop
[Desktop Entry]
Type=Application
Name=onboard
Exec=onboard
EOF
}

install_grandorgue () {
  echo ">>> Install GrandOrgue..."
  ./update-grandorgue.sh
  sudo cp ./update-grandorgue.sh /usr/local/bin/update-grandorgue
  cat <<EOF >${AUTOSTART}/GrandOrgue.desktop
[Desktop Entry]
Type=Application
Name=GrandOrgue
Exec=GrandOrgue
EOF
}

update_system
${HAS_ARGONONE} && setup_argonone
${HAS_WAVESHARE_LCD7} && setup_waveshare_lcd7
install_grandorgue ${GRANDORGUE_RELEASE}
cleanup_system
