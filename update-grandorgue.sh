#!/bin/sh
ORG=GrandOrgue
PROJECT=grandorgue
LATEST_RELEASE=$(curl -L -s -H 'Accept: application/json' https://github.com/${ORG}/${PROJECT}/releases/latest)
LATEST_VERSION=$(echo ${LATEST_RELEASE} | sed -e 's/.*"tag_name":"\([^"]*\)".*/\1/')
ARTIFACT_URL="https://github.com/${ORG}/${PROJECT}/releases/download/${LATEST_VERSION}/${PROJECT}_${LATEST_VERSION}_arm64.deb"
wget ${ARTIFACT_URL} -O /tmp/${PROJECT}_${LATEST_VERSION}_arm64.deb
sudo apt install -y /tmp/${PROJECT}_${LATEST_VERSION}_arm64.deb
